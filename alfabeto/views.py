from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.urls import reverse, reverse_lazy
from django.shortcuts import render, get_object_or_404
from .models import Post, Comment, Category
from .forms import CommentForm, KanjiForm
from django import forms
from django.views import generic


class KanjiListView(generic.ListView):
    model = Post
    context_object_name = 'kanji_list'
    template_name = 'alfabeto/index.html'

# def list_kanji(request):
#     kanji_list = Post.objects.all()
#     context = {'kanji_list': kanji_list}
#     return render(request, 'alfabeto/index.html', context)

class KanjiDetailView(generic.DetailView):
    model = Post
    context_object_name = 'kanji'
    template_name = 'alfabeto/detail.html'

# def detail_kanji(request, kanji_id):
#     kanji = get_object_or_404(Post, pk=kanji_id)
#     context = {'kanji': kanji}
#     return render(request, 'alfabeto/detail.html', context)

class KanjiCreateView(generic.CreateView):
    context_object_name = 'kanji'
    form_class = KanjiForm
    model = Post
    template_class = KanjiForm
    template_name = "alfabeto/create.html"

    def get_success_url(self):
        return reverse('alfabeto:detail', kwargs={'pk': self.object.id})


# def create_kanji(request):
#     if request.method == 'POST':
#         form = KanjiForm(request.POST)
#         kanji_name = request.POST['name']
#         if form.is_valid():
#             kanji_img_url = request.POST['img_url']
#             kanji_trad = request.POST['trad']
#             kanji = Post(name= kanji_name,
#                         img_url= kanji_img_url,
#                         trad = kanji_trad)
#             kanji.save()
#             return HttpResponseRedirect(
#                 reverse('alfabeto:detail', args=(kanji.id, )))
#     else:
#         form = KanjiForm()
#         context = {'form': form}
#         return render(request, 'alfabeto/create.html', context)    
    
class KanjiUpdateView(generic.UpdateView):
    context_object_name = 'kanji'
    form_class = KanjiForm
    model = Post
    template_class = KanjiForm
    template_name = "alfabeto/update.html"

    def get_success_url(self):
        return reverse('alfabeto:detail', kwargs={'pk': self.object.id})

# def update_kanji(request, kanji_id):
#     kanji = get_object_or_404(Post, pk=kanji_id)
#     if request.method == "POST":
#         form = KanjiForm(request.POST)
#         if form.is_valid():
#             kanji.name = request.POST['name']
#             kanji.img_url = request.POST['img_url']
#             kanji.trad = request.POST['trad']
#             kanji.save()
#             return HttpResponseRedirect(
#                 reverse('alfabeto:detail', args=(kanji.id, )))
#     else:
#         form = KanjiForm(
#             initial={
#                 'name': 'Kanji',
#                 'img_url': 'URL do Kanji',
#                 'trad': 'Tradução',
#             })

#         context = {'kanji': kanji, 'form': form}
#         return render(request, 'alfabeto/update.html', context)

class KanjiDeleteView(generic.DeleteView):
    model = Post
    context_object_name = 'kanji'
    template_name = 'alfabeto/delete.html'
    def get_success_url(self):
        return reverse('alfabeto:index')

# def delete_kanji(request, kanji_id):
#     kanji = get_object_or_404(Post, pk=kanji_id)

#     if request.method == "POST":
#         kanji.delete()
#         return HttpResponseRedirect(reverse('alfabeto:index'))

#     context = {'kanji': kanji}
#     return render(request, 'alfabeto/delete.html', context)

def create_comment(request, kanji_id):
    kanji = get_object_or_404(Post, pk=kanji_id)
    if request.method == 'POST':
        form = CommentForm(request.POST)
        if form.is_valid():
            comment_author = form.cleaned_data['author']
            comment_text = form.cleaned_data['text']
            comment = Comment(author=comment_author,
                            text=comment_text,
                            name=kanji)
            comment.save()
            return HttpResponseRedirect(
                reverse('alfabeto:detail', args=(kanji_id, )))
    else:
        form = CommentForm()
    context = {'form': form, 'kanji': kanji}
    return render(request, 'alfabeto/comment.html', context)

class CategoryView(generic.ListView):
    model = Category
    template_name = 'alfabeto/categorylist.html'

def CategoryIndividual(request, category_id):
    category = get_object_or_404(Category, pk = category_id)
    context = {'category' : category}
    return render(request, 'alfabeto/category.html', context)

class CategoryCreateView(generic.CreateView):
    model = Category
    template_name = 'alfabeto/create-category.html'
    fields = ['name', 'author', 'kanji']
    success_url = reverse_lazy('alfabeto:category')

