from django.db import models
from django.conf import settings


class Post(models.Model):
    name = models.CharField(max_length=255)
    data = models.DateTimeField(auto_now=True)
    img_url = models.URLField(max_length=200, null=True)
    trad = models.CharField(max_length=255)

    def __str__(self):
        return f'{self.name} ({self.data})'

class Comment(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL,
                               on_delete=models.CASCADE)
    text = models.CharField(max_length=255)
    data = models.DateTimeField(auto_now=True)
    name = models.ForeignKey(Post, on_delete=models.CASCADE)
    
    def __str__(self):
        return f'"{self.text}" - {self.author.username}'

class Category(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL,
                               on_delete=models.CASCADE)
    name = models.CharField(max_length=255)
    kanji = models.ManyToManyField(Post)

    def __str__(self):
        return f'{self.name} by {self.author}'