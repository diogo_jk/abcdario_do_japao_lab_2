from django.apps import AppConfig


class AlfabetoConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'alfabeto'
