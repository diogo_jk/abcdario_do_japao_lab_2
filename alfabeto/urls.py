from django.urls import path

from . import views

app_name = 'alfabeto'
urlpatterns = [
    path('', views.KanjiListView.as_view(), name='index'),
    path('create/', views.KanjiCreateView.as_view(), name='create'),
    path('<int:pk>/', views.KanjiDetailView.as_view(), name='detail'),
    path('update/<int:pk>/', views.KanjiUpdateView.as_view(), name='update'),
    path('delete/<int:pk>/', views.KanjiDeleteView.as_view(), name='delete'),
    path('<int:kanji_id>/review/', views.create_comment, name='comment'),
    path('category/', views.CategoryView.as_view(), name='categorylist'),
    path('category/<int:category_id>', views.CategoryIndividual, name='category'),
    path('category/create', views.CategoryCreateView.as_view(), name='create-category'),
]