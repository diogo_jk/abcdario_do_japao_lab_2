from django.forms import ModelForm
from .models import Post, Comment


class KanjiForm(ModelForm):
    class Meta:
        model = Post
        fields = [
            'name',
            'img_url',
            'trad',
        ]
        labels = {
            'name': 'Kanji',
            'img_url': 'URL do Kanji',
            'trad': 'Tradução',
        }

class CommentForm(ModelForm):
    class Meta:
        model = Comment
        fields = [
            'author',
            'text',
        ]
        labels = {
            'author': 'Usuário',
            'text': 'Resenha',
        }